# Surveying the Use of Ontologies in Machine Learning

## Open Problems with Ontologies <a name="open-problems"></a>
 - [Ontology Representation](#ontology-representation)
    - Ontology representation is the means by which we display, store, and conceptualize ontologies using languages and data structures.
 - [Ontology Alignment](#ontology-alignment)
    - Ontology Alignment is the matching and integration of classes from multiple ontologies using ontology structure, semantics, and/or elements.
 - [Ontology Reasoning](#ontology-reasoning)
    - Using the semantic information encoded within ontologies to make logical deductions about a conceptual space.

### Ontology Representation <a name="ontology-representation"></a>

#### “OWL 2 Web Ontology Language Structural Specification and Functional-Style Syntax (Second Edition).” Accessed October 25, 2021. https://www.w3.org/TR/owl2-syntax/.

Across the literature on ontologies, the Web Ontology Language (OWL) has been the authoritative standard for representing ontologies.

### Ontology Alignment<a name="ontology-alignment"></a>

#### Chakraborty, Jaydeep, Srividya K Bansal, Luca Virgili, Krishanu Konar, and Beyza Yaman. “Ontoconnect: Unsupervised Ontology Alignment with Recursive Neural Network.” In Proceedings of the 36th Annual ACM Symposium on Applied Computing, 1874–82, 2021.

1-sentence summary: The authors developed a tool that uses structural similarity between ontology classes and RNNs to match ontology classes.

Related work:
 - Suchanek, Fabian M., Serge Abiteboul, and Pierre Senellart. “PARIS: Probabilistic Alignment of Relations, Instances, and Schema.” ArXiv:1111.7164 [Cs], November 30, 2011. http://arxiv.org/abs/1111.7164.
 - Ngo, DuyHoa, and Zohra Bellahsene. “YAM++ : A Multi-Strategy Based Approach for Ontology Matching Task.” In Knowledge Engineering and Knowledge Management, edited by Annette ten Teije, Johanna Völker, Siegfried Handschuh, Heiner Stuckenschmidt, Mathieu d’Acquin, Andriy Nikolov, Nathalie Aussenac-Gilles, and Nathalie Hernandez, 7603:421–25. Lecture Notes in Computer Science. Berlin, Heidelberg: Springer Berlin Heidelberg, 2012. https://doi.org/10.1007/978-3-642-33876-2_38.
 - Hu, Wei, Jianfeng Chen, and Yuzhong Qu. “A Self-Training Approach for Resolving Object Coreference on the Semantic Web.” In Proceedings of the 20th International Conference on World Wide Web, 87–96. WWW ’11. New York, NY, USA: Association for Computing Machinery, 2011. https://doi.org/10.1145/1963405.1963421.

### Ontology Reasoning<a name="ontology-reasoning"></a>

#### Hohenecker, Patrick, and Thomas Lukasiewicz. “Deep Learning for Ontology Reasoning.” ArXiv:1705.10342 [Cs], May 29, 2017. http://arxiv.org/abs/1705.10342.

Brief Summary: The authors used a modified recurrent neural network to perform reasoning based on not only on feature values but also the relations between objects encoded separately.
